#include "sqlite3.h"
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>

using namespace std;

#define SQL_PATH "CarsDealer.db"

unordered_map<string, vector<string>> results;
void clearTable()
{
	for (auto it = results.begin(); it != results.end(); ++it)
	{
		it->second.clear();
	}
	results.clear();
}

void printTable()
{
	auto iter = results.end();
	iter--;
	//	int size = results.begin()->second.size();
	int size = iter->second.size();
	for (int i = -2; i < size; i++)
	{
		for (auto it = results.begin(); it != results.end(); ++it)
		{
			

			if (i == -2)
			{
				//cout << it->first << " ";
				printf("|%*s|", 13, it->first.c_str());
			}
			else if (i == -1)
				cout << "CHANGE IS COMING";
			else
				//	cout << it->second.at(i) << " ";
				printf("|%*s|", 13, it->second.at(i).c_str());			
		}

		
		cout << endl;
	}
}

int callback(void* notUsed, int argc, char** argv, char** azCol)
{
	int i;

	for (i = 0; i < argc; i++)
	{
		auto it = results.find(azCol[i]);
		if (it != results.end())
		{
			it->second.push_back(argv[i]);
		}
		else
		{
			pair<string, vector<string>> p;
			p.first = azCol[i];
			p.second.push_back(argv[i]);
			results.insert(p);
		}
	}

	return 0;
}

int getLastId(void* notUsed, int argc, char** argv, char** azCol)
{
	cout << "the Last id is: " << argv[0] << endl;
	return 0;
}


string runCommand(sqlite3* db, string sql, char* zErrMsg, bool update)
{
	int rc;
	bool flag = true;

	rc = sqlite3_exec(db, sql.c_str(), callback, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 0;
	}
	else if (!update)
	{
		auto it = results.begin();
		cout << it->second.at(0) << endl;
		return it->second.at(0);
		
	}

	return " ";
}

bool carPurchase(int buyerid, int carid, sqlite3* db, char* zErrMsg)
{
	
	int balanceAfter = 0;
	
	string balance = " ";
	string available = " ";
	string carprice = " ";

	string sql = "select balance from accounts where id = " + to_string(buyerid);
	//cout << sql << endl; // DEBUGGING

	clearTable();

	balance = runCommand(db, sql, zErrMsg, false);

	sql = "select price from cars where id = " + to_string(carid);

	clearTable();

	carprice = runCommand(db, sql, zErrMsg, false);

	sql = "select available from cars where id = " + to_string(carid);

	clearTable();

	available = runCommand(db, sql, zErrMsg, false);

	balanceAfter = stoi(balance) - stoi(carprice);

	if (balanceAfter >= 0 && available != "2")
	{		
		
		cout << "THIS JUST HAPPENED ! " << endl;
		sql = "update accounts set balance =" + to_string(balanceAfter) + " where id = " + to_string(buyerid);
		runCommand(db, sql, zErrMsg, true);
		
		clearTable();

		sql = "update cars set available = 2 where id = " + to_string(carid);
		runCommand(db, sql, zErrMsg, true);

		clearTable();


		return true;
	}

	clearTable();

	return false;
}

bool balanceTransfer(int from, int to, int amount, sqlite3* db, char* zErrMsg)
{
	string fromBal, toBal = " ";
	int temp = 0;

	clearTable();

	string sql = "select balance from accounts where id = " + to_string(from);
	fromBal = runCommand(db, sql, zErrMsg, false);

	clearTable();

	sql = "select balance from accounts where id = " + to_string(to);
	toBal = runCommand(db, sql, zErrMsg, false);

	clearTable();

	if (stoi(fromBal) - stoi(toBal) >= amount)
	{
		temp = stoi(fromBal) - amount;

		sql = "UPDATE accounts SET balance = " + to_string(temp) + " WHERE id = " + to_string(from);
		runCommand(db, sql, zErrMsg, true);

		clearTable();

		temp = stoi(toBal) + amount;
		sql = "UPDATE accounts SET balance = " + to_string(temp) + " WHERE id = " + to_string(to);
		runCommand(db, sql, zErrMsg, true);

		clearTable();

		return true;
	}

	return false;

}

int main()
{
	int rc;
	sqlite3* db;
	char *zErrMsg = 0;
	bool flag = true;

	// connection to the database
	rc = sqlite3_open(SQL_PATH, &db);

	if (rc)
	{
		cout << "Can't open database: " << sqlite3_errmsg(db) << endl;
		sqlite3_close(db);
		system("Pause");
		return(1);
	}

	system("CLS");	

	clearTable();

	//carPurchase(1, 7, db, zErrMsg);
	//carPurchase(8, 8, db, zErrMsg);
	//carPurchase(5, 2, db, zErrMsg);

	balanceTransfer(1, 2, -5000, db, zErrMsg);
	
	system("pause");

	sqlite3_close(db);

}