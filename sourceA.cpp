#include "sqlite3.h"
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>

#define SQL_DB_NAME "FirstPart.db"

using namespace std;

string addName(string name)
{
	static int id = 1;
	string sql;
	sql = "INSERT INTO PEOPLE(ID, NAME) VALUES(" + to_string(id) + ", '" + name + " ');";

	id++;

	return sql;
};

static int callback(void *unused, int count, char **data, char **columns)
{
	int idx;

	printf("There are %d column(s)\n", count);

	for (idx = 0; idx < count; idx++) {
		printf("The data in column \"%s\" is: %s\n", columns[idx], data[idx]);
	}

	printf("\n");

	return 0;
}

int main()
{

	int rc;
	sqlite3* db;
	char *zErrMsg = 0;
	bool flag = true;

	char *sql;

	// connection to the database
	rc = sqlite3_open(SQL_DB_NAME, &db);

	if (rc)
	{
		cout << "Can't open database: " << sqlite3_errmsg(db) << endl;
		sqlite3_close(db);
		system("Pause");
		return(1);
	}

	// Creating the 'People' Table

	sql = "CREATE TABLE PEOPLE(ID INT PRIMARY KEY NOT NULL, NAME TEXT NOT NULL);";

	rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	else cout << "Table \'People\' created succesfully\n";

	rc = sqlite3_exec(db, (addName("Paul") + addName("Jhon") + addName("Jeff")).c_str(), callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	else cout << "Added successfully\n";

	sql = "UPDATE PEOPLE SET NAME = 'Moti' WHERE ID = 3;";

	rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}

	sqlite3_close(db);

	system("PAUSE");
	return 0;
}

